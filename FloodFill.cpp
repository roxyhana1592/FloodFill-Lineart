//
// Created by Roxy on 02/06/2017.
//

#include "FloodFill.h"
#include "pgm.hpp"

void FloodFill::load(std::string path) {
    image = pgmread((char *) path.c_str(), &width, &height);
}

unsigned char FloodFill::getPixel(int x, int y) {
    return image[x][y];
}

void FloodFill::setPixel(unsigned char pixel, int x, int y) {
    image[x][y] = pixel;
}

unsigned char FloodFill::randomIntensity() {
    return (unsigned char) ((rand() % 150) + 50);
}

void FloodFill::floodfill(int x, int y, unsigned char oldIntensity, unsigned char newIntensity) {
    if(x<0 || y<0 || x>=width || y>=height){
        return;
    }

    if(getPixel(x,y) != oldIntensity){
        return;
    }

    setPixel(newIntensity,x,y);
    floodfill(x+1,y,oldIntensity,newIntensity);
    floodfill(x,y+1,oldIntensity,newIntensity);
    floodfill(x-1,y,oldIntensity,newIntensity);
    floodfill(x,y-1,oldIntensity,newIntensity);

}

void FloodFill::fillAll() {
    for (int x=0; x<width; x++){
        for (int y=0; y<height; y++){
            if(getPixel(x,y)==WHITE){
                unsigned char intesity = randomIntensity();
                floodfill(x,y,WHITE,intesity);
            }
        }
    }

}

void FloodFill::save(std::string path) {
    pgmwrite(image, (char *) path.c_str(), width, height);
}

FloodFill::~FloodFill() {

}
