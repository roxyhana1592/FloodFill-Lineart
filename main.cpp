#include <iostream>
#include <time.h>
#include "FloodFill.h"


int main() {

    srand((unsigned int) time(NULL));
    FloodFill image;

    image.load("lineart.pgm");

    image.fillAll();

    image.save("filled.pgm");

}

