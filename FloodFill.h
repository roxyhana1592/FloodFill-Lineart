//
// Created by Roxy on 02/06/2017.
//

#ifndef FLOODFILL_FLOODFILL_H
#define FLOODFILL_FLOODFILL_H
#include <string>

#define WHITE   255

class FloodFill {
    int width;
    int height;
    unsigned char** image;

public:
    ~FloodFill();

    void load(std::string path);
    void save(std::string path);

    unsigned char getPixel(int x, int y);
    void setPixel(unsigned char pixel, int x, int y);

    unsigned char randomIntensity();

    void floodfill(int x, int y, unsigned char oldIntensity, unsigned char newIntensity);

    void fillAll();
};


#endif //FLOODFILL_FLOODFILL_H
